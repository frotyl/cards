package net.frotyl.mobile.android.cards;

import net.frotyl.mobile.android.cards.gdata.Card;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static net.frotyl.mobile.android.cards.MatchCards.MATCH.BLIZNIAKI;
import static net.frotyl.mobile.android.cards.MatchCards.MATCH.FIGURY;
import static net.frotyl.mobile.android.cards.MatchCards.MATCH.KOLOR;
import static net.frotyl.mobile.android.cards.MatchCards.MATCH.SCHODKI;
import static org.junit.Assert.assertEquals;

public class MatchCardsTest extends MatchCards {

    private static List<Card> getCardsList(String... items) {
        ArrayList<Card> cards = new ArrayList<>(5);
        if (items != null) {
            for (String item : items) {
                cards.add(new Card(null, null, null, item));
            }
        }
        return cards;
    }


    // kolors: H, D, C, S
    private static List<Card> CHECK_KOLOR_TRUE = getCardsList("2C", "4C", "5C", "AC", "QC");
    private static List<Card> CHECK_KOLOR_FALSE = getCardsList("2H", "4H", "5D", "AD", "QC");
    private static List<Card> CHECK_KOLOR_FALSE_2 = getCardsList("2X", "4Y", "5Z", "AD", "QC");

    private static List<Card> CHECK_SCHODKI_TRUE = getCardsList("2H", "4H", "5D", "AD", "3C");
    private static List<Card> CHECK_SCHODKI_FALSE = getCardsList("2H", "4H", "5D", "AD", "QC");

    private static List<Card> CHECK_FIGURY_TRUE = getCardsList("KH", "4H", "QD", "AD", "3C");
    private static List<Card> CHECK_FIGURY_FALSE = getCardsList("2H", "4H", "5D", "AD", "QC");

    private static List<Card> CHECK_BLIZNIAKI_TRUE = getCardsList("KH", "AH", "QD", "AD", "AC");
    private static List<Card> CHECK_BLIZNIAKI_TRUE_2 = getCardsList("7C", "7S", "QC", "7H", "QS");
    private static List<Card> CHECK_BLIZNIAKI_FALSE = getCardsList("2H", "4H", "5D", "AD", "QC");

    private static List<Card> CHECK_KOLOR_BLIZNIAKI_FIGURY_TRUE = getCardsList("2C", "3C", "QC", "QH", "QS");

    @Test
    public void checkKolors() throws Exception {
        List<MATCH> matches = MatchCards.findMatch(CHECK_KOLOR_TRUE);
        assertEquals(Boolean.TRUE, gotMatch(matches, KOLOR));

        matches = MatchCards.findMatch(CHECK_KOLOR_FALSE);
        assertEquals(Boolean.FALSE, gotMatch(matches, KOLOR));

        matches = MatchCards.findMatch(CHECK_KOLOR_FALSE_2);
        assertEquals(Boolean.FALSE, gotMatch(matches, KOLOR));

    }

    @Test
    public void checkSchodki() throws Exception {
        List<MATCH> matches = MatchCards.findMatch(CHECK_SCHODKI_TRUE);
        assertEquals(Boolean.TRUE, gotMatch(matches, SCHODKI));

        matches = MatchCards.findMatch(CHECK_SCHODKI_FALSE);
        assertEquals(Boolean.FALSE, gotMatch(matches, SCHODKI));
    }

    @Test
    public void checkFigury() throws Exception {
        List<MATCH> matches = MatchCards.findMatch(CHECK_FIGURY_TRUE);
        assertEquals(Boolean.TRUE, gotMatch(matches, FIGURY));

        matches = MatchCards.findMatch(CHECK_FIGURY_FALSE);
        assertEquals(Boolean.FALSE, gotMatch(matches, FIGURY));
    }

    @Test
    public void checkBlizniaki() throws Exception {
        List<MATCH> matches = MatchCards.findMatch(CHECK_BLIZNIAKI_TRUE);
        assertEquals(Boolean.TRUE, gotMatch(matches, BLIZNIAKI));

        matches = MatchCards.findMatch(CHECK_BLIZNIAKI_TRUE_2);
        assertEquals(Boolean.TRUE, gotMatch(matches, BLIZNIAKI));

        matches = MatchCards.findMatch(CHECK_BLIZNIAKI_FALSE);
        assertEquals(Boolean.FALSE, gotMatch(matches, BLIZNIAKI));
    }

    @Test
    public void checkKolorBlizniakiFigury() throws Exception {
        List<MATCH> matches = MatchCards.findMatch(CHECK_KOLOR_BLIZNIAKI_FIGURY_TRUE);
        assertEquals(Boolean.TRUE, gotMatch(matches, KOLOR));
        assertEquals(Boolean.TRUE, gotMatch(matches, BLIZNIAKI));
        assertEquals(Boolean.TRUE, gotMatch(matches, FIGURY));
        assertEquals(Boolean.FALSE, gotMatch(matches, SCHODKI));
    }

    private boolean gotMatch(List<MATCH> matches, MATCH match) {
        for (MATCH item : matches) {
            if (match.equals(item)) {
                return true;
            }
        }
        return false;
    }
}
