package net.frotyl.mobile.android.cards;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.frotyl.mobile.android.cards.gdata.Card;
import net.frotyl.mobile.android.cards.gdata.Deck;
import net.frotyl.mobile.android.cards.gdata.DrawCards;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TheStoryStartsHere extends AppCompatActivity {

    @BindView(R.id.button)
    Button btGetCards;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.card1)
    ImageView card1;
    @BindView(R.id.card2)
    ImageView card2;
    @BindView(R.id.card3)
    ImageView card3;
    @BindView(R.id.card4)
    ImageView card4;
    @BindView(R.id.card5)
    ImageView card5;

    @BindView(R.id.cards)
    LinearLayout cardsLayout;

    private Bitmap cardB1, cardB2, cardB3, cardB4, cardB5;

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_story_starts_here);
        ButterKnife.bind(this);

        loadSpinnerIdTypes();
        loadBitmapsFromBundle(savedInstanceState);
    }

    private void loadBitmapsFromBundle(Bundle inState) {
        if (inState != null) {
            cardB1 = loadBitmapsFromBundle(inState, StaticData.CARD_1);
            cardB2 = loadBitmapsFromBundle(inState, StaticData.CARD_2);
            cardB3 = loadBitmapsFromBundle(inState, StaticData.CARD_3);
            cardB4 = loadBitmapsFromBundle(inState, StaticData.CARD_4);
            cardB5 = loadBitmapsFromBundle(inState, StaticData.CARD_5);
        }
    }

    private Bitmap loadBitmapsFromBundle(Bundle inState, String key) {
        return inState.getParcelable(key);
    }

    private void updateCardsView() {
        BitmapUtil.updateCardView(card1, cardB1, cardsViewWidth, cardsViewHeight);
        BitmapUtil.updateCardView(card2, cardB2, cardsViewWidth, cardsViewHeight);
        BitmapUtil.updateCardView(card3, cardB3, cardsViewWidth, cardsViewHeight);
        BitmapUtil.updateCardView(card4, cardB4, cardsViewWidth, cardsViewHeight);
        BitmapUtil.updateCardView(card5, cardB5, cardsViewWidth, cardsViewHeight);
    }

    int cardsViewWidth;
    int cardsViewHeight;

    @Override
    protected void onResume() {
        super.onResume();
        initProgressDialog();

        cardsLayout.post(new Runnable() {
            @Override
            public void run() {
                cardsViewWidth = cardsLayout.getWidth();
                cardsViewHeight = cardsLayout.getHeight();
                updateCardsView();
            }
        });
        hideProgress();
    }

    private void initProgressDialog() {
        progress = new ProgressDialog(this);
        progress.setTitle(R.string.get_data_title);
        progress.setMessage(getString(R.string.get_data_message));
        progress.setCancelable(false);
    }

    private void showProgress() {
        progress.show();
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        putBitmapAsParcelToBundle(outState, StaticData.CARD_1, card1);
        putBitmapAsParcelToBundle(outState, StaticData.CARD_2, card2);
        putBitmapAsParcelToBundle(outState, StaticData.CARD_3, card3);
        putBitmapAsParcelToBundle(outState, StaticData.CARD_4, card4);
        putBitmapAsParcelToBundle(outState, StaticData.CARD_5, card5);
    }

    private void putBitmapAsParcelToBundle(Bundle outState, String key, ImageView card) {
        if (card != null && card.getDrawable() != null) {
            outState.putParcelable(key, ((BitmapDrawable) card.getDrawable()).getBitmap());
        }
    }

    private int deckCounts = StaticData.DECK_COUNT;
    private boolean newNewDeck = false;

    @OnItemSelected(R.id.spinner)
    void onItemSelected(int position) {
        // position is starting from 0
        int deckCountPrev = deckCounts;
        deckCounts = ++position;
        if (deckCountPrev != deckCounts) {
            newNewDeck = true;
        }
    }

    private void loadSpinnerIdTypes() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deck_numbers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(4);
    }


    @OnClick(R.id.button)
    void getNewShuffle() {
        if (NetUtil.isOnline(this)) {
            setEnableClickers(Boolean.FALSE);
            Gson gson = new GsonBuilder().create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(StaticData.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            final APIInterface service = retrofit.create(APIInterface.class);

            if (newNewDeck || DataSingleton.getDeckId().length() == 0) {
                newNewDeck = false;
                showProgress();
                Call<Deck> call = service.getNewDeck(deckCounts);
                call.enqueue(new Callback<Deck>() {
                    @Override
                    public void onResponse(Call<Deck> call, Response<Deck> response) {
                        DataSingleton.updateDeckId(response.body().getDeckId());
                        DataSingleton.updateRemaining(response.body().getRemaining());

                        getCards(service);
                    }

                    @Override
                    public void onFailure(Call<Deck> call, Throwable t) {
                        hideProgress();
                        resetStateAndShowMsg();
                    }
                });
            } else if (DataSingleton.getDeckId().length() > 0 && DataSingleton.getRemaining() < StaticData.CARDS_COUNT) {
                Call<Deck> call = service.reshuffle(DataSingleton.getDeckId());
                call.enqueue(new Callback<Deck>() {
                    @Override
                    public void onResponse(Call<Deck> call, Response<Deck> response) {
                        DataSingleton.updateDeckId(response.body().getDeckId());
                        DataSingleton.updateRemaining(response.body().getRemaining());

                        getCards(service);
                    }

                    @Override
                    public void onFailure(Call<Deck> call, Throwable t) {
                        hideProgress();
                        resetStateAndShowMsg();
                    }
                });
            } else {
                showProgress();
                getCards(service);
            }
        } else {
            showToast(R.string.brak_polaczenia);
        }
    }

    private void resetStateAndShowMsg() {
        DataSingleton.updateDeckId(StaticData.EMPTY);
        showToast(R.string.cos_poszlo_nie_tak);
    }


    private String resultMessageToShow;

    private void getCards(APIInterface service) {
        Call<DrawCards> call2 = service.drawCards(DataSingleton.getDeckId(), StaticData.CARDS_COUNT);

        call2.enqueue(new Callback<DrawCards>() {
            @Override
            public void onResponse(Call<DrawCards> call, Response<DrawCards> response) {
                Log.d("CARDS", response.body().toString());

                if (response.body().isSuccess()) {
                    DataSingleton.updateRemaining(response.body().getRemaining());
                }

                List<Card> cards = response.body().getCards();

                List<MatchCards.MATCH> matches = MatchCards.findMatch(cards);
                String messageMatch;

                if (matches == null || matches.size() == 0) {
                    messageMatch = getString(R.string.ulozenia_brak);
                } else {
                    int count = 0;
                    StringBuffer sb = new StringBuffer(getString(R.string.ulozenie));
                    for (MatchCards.MATCH match : matches) {
                        sb.append((count > 0 ? ", " : "") + getString(match.getResId()));
                        count++;
                    }
                    messageMatch = sb.toString();
                }

                TheStoryStartsHere.this.resultMessageToShow = messageMatch;

                updateCards(card1, cards.get(0).getImage());
                updateCards(card2, cards.get(1).getImage());
                updateCards(card3, cards.get(2).getImage());
                updateCards(card4, cards.get(3).getImage());
                updateCards(card5, cards.get(4).getImage());
            }

            @Override
            public void onFailure(Call<DrawCards> call, Throwable t) {
                hideProgress();
                resetStateAndShowMsg();
            }
        });
    }

    private void updateCards(final ImageView card, String wwwLink) {
        new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... strings) {
                try {
                    URL url = new URL(strings[0]);
                    Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    return BitmapUtil.rescaleBitmap(bitmap, cardsViewWidth, cardsViewHeight);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                if (bitmap == null) {
                    showToast(R.string.cos_poszlo_nie_tak);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            card.setImageBitmap(bitmap);
                            card.invalidate();
                        }
                    });
                }
                updateCardsResult();
            }
        }.execute(wwwLink);
    }

    private void showToast(int resId) {
        Toast.makeText(getApplicationContext(), resId, Toast.LENGTH_SHORT).show();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private int currentCardsAlreadyUpdated = 0;

    private void updateCardsResult() {
        currentCardsAlreadyUpdated++;
        if (StaticData.CARDS_COUNT > currentCardsAlreadyUpdated) {
            setEnableClickers(Boolean.TRUE);
            hideProgress();
            showToast(resultMessageToShow);
        }
    }

    private void setEnableClickers(boolean enable) {
        btGetCards.setEnabled(enable);
        btGetCards.setClickable(enable);
        spinner.setEnabled(enable);
        spinner.setClickable(enable);
        if (Boolean.FALSE == enable) {
            currentCardsAlreadyUpdated = 0;
        }
    }
}
