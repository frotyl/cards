package net.frotyl.mobile.android.cards.gdata;

import com.google.gson.annotations.SerializedName;

public class Deck {

    @SerializedName("success")
    private boolean success;

    @SerializedName("deck_id")
    private String deckId;

    @SerializedName("shuffled")
    private boolean shuffled;

    @SerializedName("remaining")
    private int remaining;

    public Deck(boolean success, String deckId, boolean shuffled, int remaining) {
        this.success = success;
        this.deckId = deckId;
        this.shuffled = shuffled;
        this.remaining = remaining;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getDeckId() {
        return deckId;
    }

    public boolean isShuffled() {
        return shuffled;
    }

    public int getRemaining() {
        return remaining;
    }

    @Override
    public String toString() {
        return "Deck{" +
                "success=" + success +
                ", deckId='" + deckId + '\'' +
                ", shuffled=" + shuffled +
                ", remaining=" + remaining +
                '}';
    }
}
