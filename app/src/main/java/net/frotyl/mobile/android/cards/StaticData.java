package net.frotyl.mobile.android.cards;

public interface StaticData {
    String BASE_URL = "http://deckofcardsapi.com";
    int DECK_COUNT = 5;
    int CARDS_COUNT = 5;

    String EMPTY = "";

    String CARD_1 = "c1";
    String CARD_2 = "c2";
    String CARD_3 = "c3";
    String CARD_4 = "c4";
    String CARD_5 = "c5";

}
