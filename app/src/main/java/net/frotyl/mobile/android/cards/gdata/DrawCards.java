package net.frotyl.mobile.android.cards.gdata;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DrawCards {
    @SerializedName("remaining")
    private int remaining;

    @SerializedName("cards")
    private List<Card> cards;

    @SerializedName("success")
    private boolean success;

    @SerializedName("deck_id")
    private String deckId;


    public DrawCards(int remaining, List<Card> cards, boolean success, String deckId) {
        this.remaining = remaining;
        this.cards = cards;
        this.success = success;
        this.deckId = deckId;
    }

    public int getRemaining() {
        return remaining;
    }

    public List<Card> getCards() {
        return cards;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getDeckId() {
        return deckId;
    }

    @Override
    public String toString() {
        return "DrawCards{" +
                "remaining=" + remaining +
                ", cards=" + cards +
                ", success=" + success +
                ", deckId='" + deckId + '\'' +
                '}';
    }
}