package net.frotyl.mobile.android.cards;

class DataSingleton {

    private static DataSingleton instance;

    private DataSingleton() {
    }

    static DataSingleton getInstance() {
        if (instance == null) {
            instance = new DataSingleton();
        }
        return instance;
    }

    private String deck_id = "";
    private int remaining;


    static void updateDeckId(String deck_id) {
        getInstance().deck_id = deck_id;
    }

    static String getDeckId() {
        return getInstance().deck_id;
    }

    static void updateRemaining(int remaining) {
        getInstance().remaining = remaining;
    }

    static int getRemaining() {
        return getInstance().remaining;
    }
}
