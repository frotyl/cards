package net.frotyl.mobile.android.cards;

import net.frotyl.mobile.android.cards.gdata.Deck;
import net.frotyl.mobile.android.cards.gdata.DrawCards;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface APIInterface {

    @GET("/api/deck/new/shuffle/")
    Call<Deck> getNewDeck(@Query("deck_count") int deckCount);

    @GET("/api/deck/{deck_id}/shuffle/")
    Call<Deck> reshuffle(@Path("deck_id") String deck_id);

    @GET("/api/deck/{deck_id}/draw/")
    Call<DrawCards> drawCards(@Path("deck_id") String deck_id, @Query("count") int cards);

}
