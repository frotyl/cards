package net.frotyl.mobile.android.cards.gdata;

import com.google.gson.annotations.SerializedName;

public class Card {

    @SerializedName("image")
    private String image;

    @SerializedName("value")
    private String value;

    @SerializedName("suit")
    private String suit;

    @SerializedName("code")
    private String code;


    public Card(String image, String value, String suit, String code) {
        this.image = image;
        this.value = value;
        this.suit = suit;
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public String getValue() {
        return value;
    }

    public String getSuit() {
        return suit;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Card{" +
                "image='" + image + '\'' +
                ", value='" + value + '\'' +
                ", suit='" + suit + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
