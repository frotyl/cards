package net.frotyl.mobile.android.cards;

import net.frotyl.mobile.android.cards.gdata.Card;

import java.util.ArrayList;
import java.util.List;

public class MatchCards {

    enum MATCH {
        KOLOR(R.string.KOLOR),
        SCHODKI(R.string.SCHODKI),
        FIGURY(R.string.FIGURY),
        BLIZNIAKI(R.string.BLIZNIAKI);

        private final int resId;

        MATCH(int resId) {
            this.resId = resId;
        }

        public int getResId() {
            return resId;
        }
    }

    private static Integer codeToValue(String code) {
        if (code == null || code.length() == 0) {
            return null;
        }
        switch (code.charAt(0)) {
            case 'A':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case '0':
                return 10; // 1(0)
            case 'J':
                return 11; // (J)ack
            case 'Q':
                return 12; // (Q)uen
            case 'K':
                return 13; // (K)ing
            default:
                return null;
        }
    }

    private static Integer codeToColor(String code) {
        if (code == null || code.length() < 2) {
            return null;
        }
        switch (code.charAt(1)) {
            case 'H':
                return 0;
            case 'D':
                return 1;
            case 'C':
                return 2;
            case 'S':
                return 3;
            default:
                return null;
        }
    }

    private static int[] kolor;
    private static int[] wartosci;
    private static ArrayList<MATCH> result;

    private static void init() {
        kolor = new int[4];
        wartosci = new int[14];
        result = new ArrayList<>();
    }

    public static List<MATCH> findMatch(List<Card> cards) {
        init();

        prepare(cards);

        findKolor();
        findSchodki();
        findFigury();
        findBlizniaki();

        return result;
    }

    private static void prepare(List<Card> cards) {
        for (Card item : cards) {
            Integer num = codeToColor(item.getCode());
            if (num != null) {
                kolor[num.intValue()]++;
            }
        }

        for (Card item : cards) {
            Integer num = codeToValue(item.getCode());
            if (num != null) {
                wartosci[num.intValue()]++;
            }
        }
    }

    private static void findKolor() {
        for (int item : kolor) {
            if (item > 2) {
                result.add(MATCH.KOLOR);
                break;
            }
        }
    }

    private static void findSchodki() {
        int count = 0;
        for (int item : wartosci) {
            if (item > 0) {
                count++;
                if (count > 2) {
                    result.add(MATCH.SCHODKI);
                    break;
                }
            } else {
                count = 0;
            }
        }
    }

    private static void findFigury() {
        int count = 0;
        count += wartosci[1];
        count += wartosci[11];
        count += wartosci[12];
        count += wartosci[13];
        if (count > 2) {
            result.add(MATCH.FIGURY);
        }
    }

    private static void findBlizniaki() {
        boolean isBlizniaki = false;
        for (int item : wartosci) {
            if (item > 2) {
                isBlizniaki = true;
                break;
            }
        }
        if (isBlizniaki) {
            result.add(MATCH.BLIZNIAKI);
        }
    }

}
