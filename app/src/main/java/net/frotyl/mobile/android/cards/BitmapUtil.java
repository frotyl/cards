package net.frotyl.mobile.android.cards;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class BitmapUtil {
    public static Bitmap rescaleBitmap(Bitmap original, int cardsWidth, int cardsHeight) {
        int remoteWidth = original.getWidth();
        int remoteHeight = original.getHeight();

        double multiWidth = 1.0 / StaticData.CARDS_COUNT;
        double ratioH = 1.0 * cardsHeight / remoteHeight;
        double ratioW = 1.0 * multiWidth * cardsWidth / remoteWidth;

        int newH;
        int newW;
        if (ratioH > ratioW) {
            newW = (int) Math.floor((remoteWidth * ratioW));
            newH = (int) Math.floor((remoteHeight * ratioW));
        } else {
            newH = (int) Math.floor((remoteHeight * ratioH));
            newW = (int) Math.floor((remoteWidth * ratioH));
        }

        final Bitmap newBitmap = Bitmap.createScaledBitmap(original, newW, newH, true);
        return newBitmap;
    }

    public static void updateCardView(ImageView card, Bitmap bitmap, int cardsWidth, int cardsHeight) {
        if (bitmap != null) {
            card.setImageBitmap(BitmapUtil.rescaleBitmap(bitmap, cardsWidth, cardsHeight));
        }
    }

}
